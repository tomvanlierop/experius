const gulp = require('gulp');
const sass = require('gulp-sass');
const minify = require('gulp-minify');

// Main source file and directory to watch
const mainSourceFile = 'assets/scss/main.scss';
const sourceDirFiles = 'assets/scss/**/*.scss';

// Main script file and directory to watch
const scriptDirFiles = 'assets/js/*.js';

const cssDest = 'assets/css/';
const jsDest = 'assets/js/min/'

gulp.task('styles', function() {
  gulp.src(mainSourceFile)
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest(cssDest))
});

gulp.task('minify', function() {
  gulp.src(scriptDirFiles)
    .pipe(minify({
      ext: {
        min:'.min.js'
      },
      noSource: true
    }))
    .pipe(gulp.dest(jsDest))
});

gulp.task('watch', function() {
  gulp.watch(sourceDirFiles)
    .on('change', gulp.series('styles'))

  gulp.watch(scriptDirFiles)
    .on('change', gulp.series('minify'))
});
