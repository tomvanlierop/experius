$('.accordion-menu > .accordion-menu-item').click(function() {
    const target = $(this);

    if(target.hasClass('active')){
      target.removeClass('active');
      target.find('.accordion-items').slideUp();
    } else {
      target.addClass('active');
      target.find('.accordion-items').slideDown();
    }

  return;
});
