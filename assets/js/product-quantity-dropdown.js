$('.product-quantity-dropdown').click(function() {
  $(this).toggleClass('dropdown--open');
});

$('.product-quantity-dropdown-list li').click(function() {
  $('.selected-quantity').html($(this).data('value'))
  $('.product-quantity-select').val($(this).data('value'));
});
