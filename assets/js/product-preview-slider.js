$('.product-thumbnail').on('click', function() {
  const sliderImageId = $(this).attr('data-image');

  $('.product-preview-image img').attr('src', sliderImageId);
});

$('.thumbnail-slider').slick({
  arrows: true,
  infinite: false,
  vertical: true,
  verticalSwiping: true,
  slidesToShow: 4,
  slidesToScroll: 4,
  prevArrow: '<i class="fa fa-chevron-up slick-arrow slick-arrow-prev" aria-disabled="true"></i>',
  nextArrow: '<i class="fa fa-chevron-down slick-arrow slick-arrow-next" aria-disabled="true"></i>',
  responsive: [
    {
      breakpoint: 992,
      settings: {
        vertical: false,
        verticalSwiping: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        prevArrow: '<i class="fa fa-angle-left slick-arrow slick-arrow-prev" aria-disabled="true"></i>',
        nextArrow: '<i class="fa fa-angle-right slick-arrow slick-arrow-next" aria-disabled="true"></i>'
      }
    }
  ]
});
