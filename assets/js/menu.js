$('.mobile-menu-toggle button').click(function(){
  if ($('.site-menu ul').hasClass('slide--in')) {
    $('.site-menu ul').removeClass('slide--in');
    $('.site-menu ul').addClass('slide--out');

    setTimeout(function () {
      $('.site-menu ul').removeClass('slide--out');
    }, 500);
  } else {
    $('.site-menu ul').addClass('slide--in');
  };
});
