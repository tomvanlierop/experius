$('.product-add-to-cart-button').click(function(e) {
  e.preventDefault();

  const chosenValues = $('.product-wrapper form').serializeArray();

  $.each(chosenValues, function(i, field) {
    $('.product-cart-values .amount').html(field.value);
  });

  $('.product-cart-feedback').fadeIn();

  setTimeout(function () {
    $('.product-cart-feedback').fadeOut();
  }, 2000);
})
